import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;


class Main {
	
	static double[] houseLocations = new double[100000];
	static int[] nodeIndexes;
	static double[] nodeLocations;
	static double[] maximums;
	static int numberOfHouses;
	static int numberOfNodes;
	
	static void printInfo()
	{
		System.out.println("House Locations: ");
		for(int i = 0; i<numberOfHouses; i++)
		{
			System.out.print(houseLocations[i] + " ");
		}
		System.out.println();
		System.out.println("Node Indexes: ");
		for(int i = 0; i<numberOfNodes; i++)
		{
			System.out.print(nodeIndexes[i] + " ");
		}
		System.out.println();
		System.out.println("Node Locations: ");
		for(int i = 0; i<numberOfNodes; i++)
		{
			System.out.print(nodeLocations[i] + " ");
		}
		System.out.println();
		System.out.println("Maximums: ");
		for(int i = 0; i<numberOfNodes; i++)
		{
			System.out.print(maximums[i] + " ");
		}
		System.out.println();
	}

	
	public static void main(String args[]) throws NumberFormatException, IOException
	{
	
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			int numberOfCases = Integer.parseInt(reader.readLine());
			
			for(int caseNumber = 0; caseNumber< numberOfCases; caseNumber++)
			{
				
				String line = reader.readLine();
				numberOfNodes = Character.getNumericValue(line.charAt(0));
				numberOfHouses = Character.getNumericValue(line.charAt(2));
				nodeLocations = new double[numberOfNodes];
				nodeIndexes = new int[numberOfNodes];
				maximums = new double[numberOfNodes];
				
				
				for(int n = 0; n<numberOfHouses; n++)
				{
					line = reader.readLine();
					houseLocations[n] = Integer.parseInt(line);
				}
			
				
				double high = houseLocations[numberOfHouses-1];
				double low = 0;
				
				while(high-low>1)
				{
					
					double midPoint = (high+low)/2;
					
					int range = 1;
					double currentRange = houseLocations[0] + (midPoint*2);
				
					for(int i =0; i<numberOfHouses; i++)
					{
						if(houseLocations[i] > currentRange)
						{
							range++;
							currentRange = houseLocations[i] + (midPoint*2);
						
						}
					}
					
					if(range > numberOfNodes)
					{
						low = midPoint;
					}
					else
					{
						high = midPoint;
					}
					
				}
				
				
	
				
				int answer = (int) (high / .5);
				System.out.println(answer*.5);
				
				
			}
			
			
			
			
			
	
		
	}

}
